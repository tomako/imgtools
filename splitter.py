from argparse import ArgumentParser
import os

from PIL import Image

EXIF_ORIENTATION_TAG = 274

SPLIT_DIR_AUTO = "auto"
SPLIT_DIR_HORIZONTAL = "horizontal"
SPLIT_DIR_VERTICAL = "vertical"


def rotate_if_required(im):
    exif = im._getexif()
    if EXIF_ORIENTATION_TAG in exif:
        if exif[EXIF_ORIENTATION_TAG] == 1:
            rotation = 0
        elif exif[EXIF_ORIENTATION_TAG] == 3:
            rotation = Image.ROTATE_180
        elif exif[EXIF_ORIENTATION_TAG] == 6:
            rotation = Image.ROTATE_270
        elif exif[EXIF_ORIENTATION_TAG] == 8:
            rotation = Image.ROTATE_90
        else:
            raise ValueError("{}: Unknown Orintation value {}".format(im.name, exif[EXIF_ORIENTATION_TAG]))
        if rotation:
            return im.transpose(rotation)
    return im


def split(im, split_dir):
    if split_dir == SPLIT_DIR_HORIZONTAL:
        box1 = (0, 0, im.width // 2, im.height)
        box2 = (im.width // 2, 0, im.width, im.height)
    else:
        box1 = (0, 0, im.width, im.height // 2)
        box2 = (0, im.height // 2, im.width, im.height)
    return im.crop(box1), im.crop(box2)


def main(input_fs_object, output_folder, split_dir):
    print("Input:", input_fs_object)
    print("Output:", output_folder)
    print("Split direction:", split_dir)
    if os.path.isdir(input_fs_object):
        inp_files = sorted(os.listdir(input_fs_object))
        input_folder = input_fs_object
    else:
        inp_files = [os.path.basename(input_fs_object)]
        input_folder = os.path.dirname(input_fs_object)
    for inp_file in inp_files:
        try:
            inp_file_path = os.path.join(input_folder, inp_file)
            if os.path.isfile(inp_file_path):
                with Image.open(inp_file_path) as im:
                    print(inp_file, im.format, "%dx%d" % im.size)
                    if "exif" in im.info:
                        im = rotate_if_required(im)
                    if split_dir == SPLIT_DIR_AUTO:
                        if im.width > im.height:
                            split_dir = SPLIT_DIR_HORIZONTAL
                        elif im.height > im.width:
                            split_dir = SPLIT_DIR_VERTICAL
                        else:
                            raise ValueError("{}: Same width and height. "
                                             "Can't determine splitting direction."
                                             .format(inp_file))
                    else:
                        if (im.width > im.height and split_dir != SPLIT_DIR_HORIZONTAL
                                or im.height > im.width and split_dir != SPLIT_DIR_VERTICAL
                                or im.height == im.width):
                            raise ValueError("{}: Splitting direction ({}) doesn't match size ({})."
                                             .format(inp_file, split_dir, im.size))
                    im1, im2 = split(im, split_dir)
                    out_file_name_chunks = os.path.splitext(os.path.basename(inp_file))
                    im1.save(os.path.join(output_folder, "{}_1{}".format(*out_file_name_chunks)))
                    im1.close()
                    im2.save(os.path.join(output_folder, "{}_2{}".format(*out_file_name_chunks)))
                    im2.close()

        except IOError:
            print(inp_file, "is not image or is not accessible")


if __name__ == "__main__":
    argparser = ArgumentParser("Image Splitter")
    argparser.add_argument("-i", "--input", required=True, dest="input_fs_object")
    argparser.add_argument("-o", "--output", required=True, dest="output_folder")
    argparser.add_argument("-d", "--direction", dest="split_dir",
                           choices=(SPLIT_DIR_AUTO, SPLIT_DIR_HORIZONTAL, SPLIT_DIR_VERTICAL),
                           default=SPLIT_DIR_AUTO,
                           help="Splitting direction (default '{}')".format(SPLIT_DIR_AUTO))

    args = argparser.parse_args()
    main(**args.__dict__)
