from argparse import ArgumentParser
import os

from PIL import Image


def main(input_folder, output_file):
    print("Input:", input_folder)
    print("Output:", output_file)
    append = False

    for inp_file in sorted(os.listdir(input_folder)):
        try:
            with Image.open(os.path.join(input_folder, inp_file)) as im:
                print(inp_file, "%dx%d" % im.size)
                append = append or os.path.isfile(output_file)
                im.save(output_file, format="PDF", append=append)

        except IOError:
            pass


if __name__ == "__main__":
    argparser = ArgumentParser("Compile images to PDF")
    argparser.add_argument("-i", "--input", required=True, dest="input_folder")
    argparser.add_argument("-o", "--output", required=True, dest="output_file")

    args = argparser.parse_args()
    main(**args.__dict__)
